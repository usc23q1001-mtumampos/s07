from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def hello_message(request):
    return HttpResponse("Hello from the views.py file!")
